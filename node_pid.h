#ifndef NODE_PID_H
#define NODE_PID_H

#include "computation_graph.h"

enum PidOperandEdges {
  PID_KP,
  PID_KD,
  PID_KI,
  PID_SETPOINT,
  PID_INPUT,
};

enum PidResultEdges {
  PID_OUTPUT,
};

struct PidState {
  double accumulation;
};

void compute_pid(struct Node *self);

void reset_pid(struct Node *self);

int graph_add_node_pid(struct ComputationGraph *graph);

#endif
