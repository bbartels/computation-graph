#include "computation_graph.h"

void print_graph(struct ComputationGraph *graph) {
  int i;
  printf("Graph:\n");
  for (i = 0; i < graph->num_nodes; i += 1) {
    printf("  %d:\n", i);
    int j;
    printf("  operand_edges:\n");
    for (j = 0; j < graph->nodes[i].num_operand_edges; j += 1) {
      if (graph->nodes[i].operand_edges[j] == NULL) continue;
      printf("  %d: %f\n", j, graph->nodes[i].operand_edges[j]->value);
    }
    printf("  result_edges:\n");
    for (j = 0; j < graph->nodes[i].num_result_edges; j += 1) {
      if (graph->nodes[i].result_edges[j] == NULL) continue;
      printf("  %d: %f\n", j, graph->nodes[i].result_edges[j]->value);
    }
    printf("  state: %f\n", *(float *) graph->nodes[i].state);
  }
}

// get a new computation_graph
struct ComputationGraph * graph_new(int max_nodes, int max_edges) {
  struct ComputationGraph *graph = malloc(sizeof(struct ComputationGraph));
  graph->nodes = malloc(sizeof(struct Node) * max_nodes);
  graph->edges = malloc(sizeof(struct Edge) * max_nodes);
  graph->num_nodes = 0;
  graph->num_edges = 0;
  return graph;
}

// create a node,
// returns the id of the node
int graph_add_node(struct ComputationGraph *graph,
                   int num_operand_edges,
                   int num_result_edges,
                   void (*compute)(struct Node *self),
                   void *state,
                   void (*reset)(struct Node *self)) {

  int id = graph->num_nodes;
  graph->num_nodes += 1;

  struct Node *node = &graph->nodes[id];

  node->operand_edges = malloc(sizeof(struct Edge *) * num_operand_edges);
  node->result_edges = malloc(sizeof(struct Edge *) * num_result_edges);
  node->num_operand_edges = num_operand_edges;
  node->num_result_edges = num_result_edges;
  node->compute = compute;
  node->state = state;
  node->reset = reset;

  return id;
}

// Removes a node based on id (remove any of its result edges)
int graph_remove_node(struct ComputationGraph *graph, int id) {
  struct Node *node = &graph->nodes[id];
  free(node->operand_edges);
  free(node->result_edges);
  free(node->state);
  int i;
  for (i = id; i < graph->num_nodes - 1; i += 1) {
    graph->nodes[id] = graph->nodes[id + 1];
  }

  return 0;
}

// Sets the result node based on given node id
// This is the entry point for the graph cmopute
int graph_set_result_node(struct ComputationGraph *graph, int id) {
  struct Node *node = &(graph->nodes[id]);
  graph->result_node = node;

  int i;
  for (i = 0; i < node->num_result_edges; i += 1) {
    int edge_id = graph->num_edges;
    graph->num_edges += 1;

    graph->edges[edge_id].source = node;
    node->result_edges[i] = &graph->edges[edge_id];
  }

  return 0;
}

// Recursively perform a DFS on the graph
int graph_compute_rec(struct Node *node) {
  int i;
  for (i = 0; i < node->num_operand_edges; i += 1) {
    struct Node *source = node->operand_edges[i]->source;
    if (!source->computed) {
      graph_compute_rec(source);
    }
  }

  node->compute(node);
  node->computed = 1;
  return 0;
}

// Compute the result of the graph (does the traversal).
// Returns non-zero if error
int graph_compute(struct ComputationGraph *graph) {
  if (graph->result_node == NULL) {
    return;
  }

  struct Node *node = graph->result_node;
  if (node->computed) {
    int i;
    for (i = 0; i < graph->num_nodes; i += 1) {
      graph->nodes[i].computed = 0;
    }
  }
  graph_compute_rec(node);


  graph->result.length = node->num_result_edges;
  graph->result.arr = malloc(sizeof(float) * node->num_result_edges);

  int i;
  for (i = 0; i < node->num_result_edges; i += 1) {
    graph->result.arr[i] = node->result_edges[i]->value;
  }

  return 0;
}

// Connect 2 nodes according to the given spec
int graph_add_edge(struct ComputationGraph *graph,
                   int src_node_id,
                   int dest_node_id,
                   int src_result_edge,
                   int dest_operand_edge) {

  struct Node *src = &graph->nodes[src_node_id];
  struct Node *dest = &graph->nodes[dest_node_id];

  if (src->result_edges[src_result_edge] == NULL) {
    int edge_id = graph->num_edges;
    graph->num_edges += 1;
    struct Edge *edge = &graph->edges[edge_id];

    src->result_edges[src_result_edge] = edge;

    graph->edges[edge_id].source = src;

    dest->operand_edges[dest_operand_edge] = edge;
  } else {
    dest->operand_edges[dest_operand_edge] = src->result_edges[src_result_edge];
  }


  return 0;
}

// Reset the state of the given node
int graph_reset_node(struct ComputationGraph *graph, int id) {
  struct Node *node = &graph->nodes[id];
  node->reset(node);
}
