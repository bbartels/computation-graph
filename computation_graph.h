#ifndef COMPUTATION_GRAPH_H
#define COMPUTATION_GRAPH_H

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

struct Node;

struct Result {
  double *arr;
  int length;
};

struct Edge {
  struct Node *source;
  double value;
} Edge;

struct EdgeMap {
  int src_result_edge;
  int dest_operand_edge;
};

struct Node {
  struct Edge **operand_edges;
  struct Edge **result_edges;
  int num_operand_edges;
  int num_result_edges;
  void (*compute)(struct Node *self);
  int computed;
  void *state;
  void (*reset)(struct Node *self);
};

struct ComputationGraph {
  struct Node *nodes;
  struct Edge *edges;
  int num_nodes;
  int num_edges;
  struct Node *result_node;
  struct Result result;
} ComputationGraph;

// get a new computation_graph
struct ComputationGraph * graph_new(int max_nodes, int max_edges);

// create a node,
// returns the id of the node
int graph_add_node(struct ComputationGraph *graph,
                   int num_operand_edges,
                   int num_result_edges,
                   void (*compute)(struct Node *self),
                   void *state,
                   void (*reset)(struct Node *self));

// Removes a node (a remove any of its result edges)
int graph_remove_node(struct ComputationGraph *graph, int id);

// Sets the result node
int graph_set_result_node(struct ComputationGraph *graph, int id);

// Compute the result of the graph (does the traversal).
// Returns non-zero if error
int graph_compute(struct ComputationGraph *graph);

// Connect 2 nodes according to the given spec
int graph_add_edge(struct ComputationGraph *graph,
                   int src_node_id,
                   int dest_node_id,
                   int src_result_edge,
                   int dest_input_edge);

// Reset the state of the given node
int graph_reset_node(struct ComputationGraph *graph, int id);


#endif
