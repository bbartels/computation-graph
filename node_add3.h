#ifndef NODE_ADD3_H
#define NODE_ADD3_H

#include "computation_graph.h"

enum Add3OperandEdges {
  ADD3_X,
  ADD3_Y,
  ADD3_Z,
};

enum Add3ResultEdges {
  ADD3_RESULT,
};

struct Add3State {
};

void compute_add3(struct Node *self);

void reset_add3(struct Node *self);

int graph_add3_node_add3(struct ComputationGraph *graph);

#endif
