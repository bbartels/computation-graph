#ifndef NODE_CONST_H
#define NODE_CONST_H

#include "computation_graph.h"

enum ConstResultEdges {
  CONST_OUTPUT,
};

void compute_const(struct Node *self);

void reset_const(struct Node *self);

int graph_add_node_const(struct ComputationGraph *graph, float constant);

#endif
