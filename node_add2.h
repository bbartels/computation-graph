#ifndef NODE_ADD2_H
#define NODE_ADD2_H

#include "computation_graph.h"

enum Add2OperandEdges {
  ADD2_X,
  ADD2_Y,
};

enum Add2ResultEdges {
  ADD2_RESULT,
};

struct Add2State {
};

void compute_add2(struct Node *self);

void reset_add2(struct Node *self);

int graph_add_node_add2(struct ComputationGraph *graph);

#endif
