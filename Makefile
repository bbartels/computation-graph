test: computation_graph.c test.c node_pid.c node_const.c node_add2.c node_add3.c node_scale.c node_accum.c
	gcc -g -o test test.c computation_graph.c node_pid.c node_const.c node_add2.c node_add3.c node_scale.c node_accum.c

.PHONY : clean
clean:
	rm test *.o *.h.gch
