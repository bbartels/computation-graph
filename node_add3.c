#include "node_add3.h"

void compute_add3(struct Node *self) {
  double x = self->operand_edges[ADD3_X]->value;
  double y = self->operand_edges[ADD3_Y]->value;
  double z = self->operand_edges[ADD3_Z]->value;
  self->result_edges[ADD3_RESULT]->value = x + y + z;
}

void reset_add3(struct Node *self) {
  // do nothing
}

int graph_add_node_add3(struct ComputationGraph *graph) {
  struct Add3State *state = malloc(sizeof(struct Add3State));
  return graph_add_node(graph,
                        3,
                        1,
                        compute_add3,
                        &state,
                        reset_add3);
}

