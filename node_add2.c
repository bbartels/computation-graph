#include "node_add2.h"

void compute_add2(struct Node *self) {
  double x = self->operand_edges[ADD2_X]->value;
  double y = self->operand_edges[ADD2_Y]->value;
  self->result_edges[ADD2_RESULT]->value = x + y;
}

void reset_add2(struct Node *self) {
  // do nothing
}

int graph_add_node_add2(struct ComputationGraph *graph) {
  struct Add2State *state = malloc(sizeof(struct Add2State));
  return graph_add_node(graph,
                        2,
                        1,
                        compute_add2,
                        &state,
                        reset_add2);
}

