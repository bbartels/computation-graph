#include <stdio.h>
#include "computation_graph.h"
#include "node_pid.h"
#include "node_const.h"
#include "node_add2.h"
#include "node_add3.h"
#include "node_scale.h"
#include "node_accum.h"
#include <math.h>
#include <pthread.h>

int eq(double a, double b)
{
  if (fabs(a-b) < 1e-5) return 1;
  return 0;
}

/*

    +----+   '2'
    | 2 0+----+    +------------+
    +----+    |    |            |
              +--->|0           |
                   |    add    0|-------- '5'
              +--->|1           |
    +----+    |    |            |
    | 3 0+----+    +------------+
    +----+   '3'

*/

int test1() {
  int ids[5];
  struct ComputationGraph *graph = graph_new(10, 20);

  // Create a constant block of 2
  ids[0] = graph_add_node_const(graph, 2);

  // Create a constant block of 3
  ids[1] = graph_add_node_const(graph, 3);

  // Create an addition block, and set as the result block
  ids[2] = graph_add_node_add2(graph);
  graph_set_result_node(graph, ids[2]);

  // Connect the constant block 2 to the addition block
  graph_add_edge(graph, ids[0], ids[2], CONST_OUTPUT, ADD2_X);

  // Connect the constant block 3 to the addition block
  graph_add_edge(graph, ids[1], ids[2], CONST_OUTPUT, ADD2_Y);

  // Compute!
  graph_compute(graph);

  double result = graph->result.arr[0];

  printf("%f\n", result);
  return !eq(result, 5);
}

/*


                '1'  +--------+  '1'   +---------------+
              +----->|0  x1  0+------->|0              |
              |      +--------+        |               |
              |                        |               |
     +-----+  | '1'  +--------+  '2'   |               |
     | 1  0+--+----->|0  x2  0|------->|1     add     0+----------- '7'
     +-----+  |      +--------+        |               |
              |                        |               |
              | '1'  +--------+  '4'   |               |
              +----->|0  x4  0|------->|2              |
                     +--------+        +---------------+


*/

int test2() {
  int ids[5];
  struct ComputationGraph *graph = graph_new(10, 20);

  ids[0] = graph_add_node_const(graph, 1);
  ids[1] = graph_add_node_scale(graph, 1);
  ids[2] = graph_add_node_scale(graph, 2);
  ids[3] = graph_add_node_scale(graph, 4);

  ids[4] = graph_add_node_add3(graph);
  graph_set_result_node(graph, ids[4]);

  graph_add_edge(graph, ids[0], ids[1], CONST_OUTPUT, SCALE_X);
  graph_add_edge(graph, ids[0], ids[2], CONST_OUTPUT, SCALE_X);
  graph_add_edge(graph, ids[0], ids[3], CONST_OUTPUT, SCALE_X);
  graph_add_edge(graph, ids[1], ids[4], SCALE_RESULT, ADD3_X);
  graph_add_edge(graph, ids[2], ids[4], SCALE_RESULT, ADD3_Y);
  graph_add_edge(graph, ids[3], ids[4], SCALE_RESULT, ADD3_Z);

  graph_compute(graph);

  double result = graph->result.arr[0];

  printf("%f\n", result);
  return !eq(result, 7);
}

/*

    +--------+   '1'  +------------------+          'state'
    | 1     0+------->|0   accumulator  0+----------
    +--------+        +------------------+
                       state += input[0]

*/
int test3() {
  int ids[5];
  struct ComputationGraph *graph = graph_new(10, 20);

  ids[0] = graph_add_node_const(graph, 1);

  ids[1] = graph_add_node_accum(graph);
  graph_set_result_node(graph, ids[1]);

  graph_add_edge(graph, ids[0], ids[1], CONST_OUTPUT, ACCUM_X);

  graph_compute(graph);
  graph_compute(graph);
  graph_compute(graph);

  double result = graph->result.arr[0];

  printf("%f\n", result);
  int failed = !eq(result, 3);

  graph_reset_node(graph, 1);

  graph_compute(graph);
  graph_compute(graph);
  graph_compute(graph);
  graph_compute(graph);

  result = graph->result.arr[0];

  printf("%f\n", result);
  return !eq(result, 4) || failed;
}

int test4() {
  int nodeIds[5];
  double states[5];
  struct ComputationGraph *graph = graph_new(10, 20);

  // kp
  nodeIds[0] = graph_add_node_const(graph, 1.0);

  // kd
  nodeIds[1] = graph_add_node_const(graph, 1.1);

  // ki
  nodeIds[2] = graph_add_node_const(graph, 1.2);

  // setpoint
  nodeIds[3] = graph_add_node_const(graph, 10);

  // input
  nodeIds[4] = graph_add_node_const(graph, 3);

  nodeIds[5] = graph_add_node_pid(graph);
  graph_set_result_node(graph, 5);

  graph_add_edge(graph, 0, 5, CONST_OUTPUT, PID_KP);
  graph_add_edge(graph, 1, 5, CONST_OUTPUT, PID_KI);
  graph_add_edge(graph, 2, 5, CONST_OUTPUT, PID_KD);
  graph_add_edge(graph, 3, 5, CONST_OUTPUT, PID_SETPOINT);
  graph_add_edge(graph, 4, 5, CONST_OUTPUT, PID_INPUT);

  graph_compute(graph);
}

int main(int argc, char *argv[]) {
  if (test1()) puts("FAILED");
  if (test2()) puts("FAILED");
  if (test3()) puts("FAILED");
  if (test4()) puts("FAILED");
}
