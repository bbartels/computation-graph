#include "node_const.h"

void compute_const(struct Node *self) {
  self->result_edges[CONST_OUTPUT]->value = *(float*) self->state;
}

void reset_const(struct Node *self) {
  // do nothing
}

int graph_add_node_const(struct ComputationGraph *graph, float constant) {
  float *state = malloc(sizeof(float));
  *state = constant;
  return graph_add_node(graph,
                        0,
                        1,
                        compute_const,
                        state,
                        reset_const);
}

