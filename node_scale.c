#include "node_scale.h"

void compute_scale(struct Node *self) {
  float x = self->operand_edges[SCALE_X]->value;
  float *scaler = (float *) self->state;
  self->result_edges[SCALE_RESULT]->value = x * *scaler;
}

void reset_scale(struct Node *self) {
  // do nothing
}

int graph_add_node_scale(struct ComputationGraph *graph, float scaler) {
  float *state = malloc(sizeof(float));
  *state = scaler;
  return graph_add_node(graph,
                        1,
                        1,
                        compute_scale,
                        state,
                        reset_scale);
}

