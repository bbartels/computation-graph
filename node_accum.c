#include "node_accum.h"

void compute_accum(struct Node *self) {
  double x = self->operand_edges[ACCUM_X]->value;
  float *total = (float *) self->state;
  *total += x;
  self->result_edges[ACCUM_RESULT]->value = *total;
}

void reset_accum(struct Node *self) {
  float *total = (float *) self->state;
  *total = 0;
}

int graph_add_node_accum(struct ComputationGraph *graph) {
  float *state = malloc(sizeof(float));
  *state = 0;
  return graph_add_node(graph,
                        1,
                        1,
                        compute_accum,
                        state,
                        reset_accum);
}

