#include "node_pid.h"

void compute_pid(struct Node *self) {
  double kp = self->operand_edges[PID_KP]->value;
  double kd = self->operand_edges[PID_KD]->value;
  double ki = self->operand_edges[PID_KI]->value;
  double setpoint = self->operand_edges[PID_SETPOINT]->value;
  double e = self->operand_edges[PID_INPUT]->value;
  // calculate

  self->result_edges[PID_OUTPUT]->value = e*kp; // and other stuff
}

void reset_pid(struct Node *self) {
  struct PidState *state = (struct PidState *) self->state;
  state->accumulation = 0;
}

int graph_add_node_pid(struct ComputationGraph *graph) {
  struct PidState *state = malloc(sizeof(struct PidState));
  return graph_add_node(graph,
                        5,
                        1,
                        compute_pid,
                        &state,
                        reset_pid);
}

