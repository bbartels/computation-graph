#ifndef NODE_ACCUM_H
#define NODE_ACCUM_H

#include "computation_graph.h"

enum AccumOperandEdges {
  ACCUM_X,
};

enum AccumResultEdges {
  ACCUM_RESULT,
};

void compute_accum(struct Node *self);

void reset_accum(struct Node *self);

int graph_add_node_accum(struct ComputationGraph *graph);

#endif
