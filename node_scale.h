#ifndef NODE_SCALE_H
#define NODE_SCALE_H

#include "computation_graph.h"

enum ScaleOperandEdges {
  SCALE_X,
};

enum ScaleResultEdges {
  SCALE_RESULT,
};

void compute_scale(struct Node *self);

void reset_scale(struct Node *self);

int graph_add_node_scale(struct ComputationGraph *graph, float scale);

#endif
